package com.example.xavier.MobileEvent.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.xavier.MobileEvent.model.Event;
import com.example.xavier.MobileEvent.dao.RoomDatabaseM2L;
import com.example.xavier.MobileEvent.R;
import com.example.xavier.MobileEvent.listAdapter.EventListAdapter;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by x
 */
public class MainActivity extends AppCompatActivity {
    private static final int CODE_RETOUR_NEW_EVENT = 1;
    private static final int CODE_RETOUR_EDIT_EVENT = 2;

    private List<Event> allEvents = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        populateListView();
        Button btnAdd = findViewById((R.id.btnAdd));
        btnAdd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i ;
                i = new Intent(getApplicationContext(),NewEventActivity.class);
                startActivityForResult(i, CODE_RETOUR_NEW_EVENT);
            }
        });
    }


    private void populateListView(){
        try {
            allEvents = new RoomDatabaseM2L.selectEvents(getApplicationContext()).execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        EventListAdapter adapter = new EventListAdapter(getApplicationContext(), allEvents);
        ListView list = findViewById(R.id.lst_evt);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long itemId) {
                Intent intent = new Intent(getApplicationContext(),EditEventActivity.class);
                intent.putExtra("codeEvt",itemId);
                startActivityForResult(intent, CODE_RETOUR_EDIT_EVENT);
            }
        });
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CODE_RETOUR_NEW_EVENT:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Bundle b = data.getExtras();
                        if(b!=null){
                            String titreEvt = b.getString("titreEvt","");
                            Event evt = new Event(titreEvt);
                            new RoomDatabaseM2L.addEvent(getApplicationContext()).execute(evt);
                            populateListView();
                        }
                        else
                            Toast.makeText(getApplicationContext(),"Enregistrement annulé",Toast.LENGTH_LONG).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getApplicationContext(),"Enregistrement annulé",Toast.LENGTH_LONG).show();
                        break;
                }
                break;
            case CODE_RETOUR_EDIT_EVENT:
                populateListView();
                break;
        }
    }
}
